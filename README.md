# Otus Rust Course

#### [Rust Developer от Otus](https://otus.ru/lessons/rust-developer/)

Репозиторий для проектов, выполненных в рамках курса. Для каждого проекта или домашнего задания будет отведено отдельная папка

## CI/CD pipelines

В этом репозитории коммит и пуш в main ветку будет запрещен. Все изменения в main ветке делаются через Merge Request (MR) из других веток. 
При открытии MR, будет запускать pipeline, который содержит в себе проверку линтером и проверку code-style.
В случае НЕуспешного результата pipeline - merge MR в main ветку будет заблокирован


## Authors and acknowledgment
Almaz Murzabekov: almaz@murzabekov.net

## License
Apache License, Version 2.0

## Project status
Personal (pet) project
